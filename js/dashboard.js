window.__gigyaConf = { 
    containerID: 'sap-cdc-container',
    onError: onError
 }
 
 function onError(e) {
    console.log(e);
}

/*
 * Account info object returned from getAccountInfo
 */
var accountInfo;

var getAccountInfoParams = {
    include: 'profile,data',
    callback: getAccountInfoCallback
}

var showScreenSetParams = {
    screenSet: 'Default-ProfileUpdate',
    startScreen: 'gigya-update-profile-screen' 
}

/*
 * Call getAccountInfo once web sdk has loaded
 */
var onGigyaServiceReady = function() {
    gigya.accounts.getAccountInfo(getAccountInfoParams);
}

function getAccountInfoCallback(response) {
    if(response.UID) {
        accountInfo = response;
        setLogoutButton();
        gigya.accounts.showScreenSet(showScreenSetParams);
    }
    else {
        location = WD;
    }
}