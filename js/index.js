window.__gigyaConf = { 
    containerID: 'sap-cdc-container',
    onError: onError
 }

var showScreenSetParams = {
    screenSet: 'Default-RegistrationLogin',
    startScreen: 'gigya-login-screen',
    authFlow: 'redirect',
    redirectURL: WD + '/dashboard.php'
}

var onGigyaServiceReady = function() {
    gigya.accounts.showScreenSet(showScreenSetParams);
}

function onError(e) {
    console.log(e);
}