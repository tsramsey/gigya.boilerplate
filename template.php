<?php include("config.php"); ?>
<!DOCTYPE html>
<html>
  <head>
    <title><?php echo $PAGE_TITLE ?></title>
    <meta name="viewport" content="width=device-width">
    <link rel="shortcut icon" type="image/png" href="<?php echo $WD ?>/images/favicon.ico"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    <link rel='stylesheet' href='<?php echo $WD ?>/css/style.css' />
  </head>
  <body>

    <nav>
        <div class="nav-wrapper">
            <a href="<?php echo $WD ?>" class="brand-logo">
                <img src="<?php echo $WD ?>/images/gigya.logo" />
            </a>
            <ul id="right-nav" class="right"></ul>
        </div>
    </nav>

    <div class="container">
        <div class="valign-wrapper full-height">
            <div class="row z-depth-2 wrapper">
                <div id="sap-cdc-container" class="col s12"></div>    
            </div>
        </div>
    </div>

    <script>
        const WD = '<?php echo $WD ?>';
    </script>

    <script src="<?php echo $WD ?>/js/global.js"></script>
    <script src="<?php echo $WD ?>/js/<?php echo $SCRIPT ?>"></script>

    <script src="https://cdns.gigya.com/js/gigya.js?apikey=<?php echo $API_KEY ?>"></script>

  </body>
</html>
