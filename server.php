<?php

include_once 'config.php';

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$gigya = [
    'apiKey' => $API_KEY,
    'baseUrl' => 'https://accounts.' . $DATA_CENTER . '.gigya.com/{0}',
    'userKey' => $USER_KEY,
    'secret' => $SECRET
];

$gigyaUrl = $gigya['baseUrl'] .
    '?apiKey=' . urlencode($gigya['apiKey']) .
    '&userKey=' . urlencode($gigya['userKey']) .
    '&secret=' . urlencode($gigya['secret']);

$url = str_replace('{0}', $_GET['endpoint'], $gigyaUrl);

$post = json_decode(file_get_contents('php://input'));

foreach ($post as $key => $value) {
    $url .= '&' . $key . '=' . urlencode( $value );
}

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$response = curl_exec($ch);
$json = json_decode($response, true);
curl_close($ch);

$res = [
    'request' => $url,
    'response' => $json
];

echo json_encode($res, true);

?>